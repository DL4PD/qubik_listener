# Qubik Listener

Listen for frames from the flowgraph via UDP and forward them to satnogs-db.

## Installation

Create a python virtual environment and install the dependencies inside:
```
mkvirtualenv qubik_listener
pip install -r requirements.txt
```

Dependencies:
- [satnogs-decoders](https://gitlab.com/librespacefoundation/satnogs/satnogs-decoders) for the (Qubik) frame structure
- [kaitaistruct](https://pypi.org/project/kaitaistruct/) for unpacking the frames


## Usage

```
usage: qubik_listener.py [-h] [--norad_id_qubik1 NORAD_ID_QUBIK1] [--norad_id_qubik2 NORAD_ID_QUBIK2] [--callsign CALLSIGN]
                         [--lat LAT] [--lon LON] [--udp_ip UDP_IP] [--udp_port UDP_PORT] [--telemetry_url TELEMETRY_URL]

Listen for frames via UDP and forward them to satnogs-db.

optional arguments:
  -h, --help            show this help message and exit
  --norad_id_qubik1 NORAD_ID_QUBIK1
                        NORAD ID, e.g. 99500
  --norad_id_qubik2 NORAD_ID_QUBIK2
                        NORAD ID, e.g. 99501
  --callsign CALLSIGN   Callsign, e.g. ZZ0ZZZ
  --lat LAT             Latitude, e.g. 23.50000E
  --lon LON             Longitude, e.g. 53.25000N
  --udp_ip UDP_IP       GRC UDP IP, e.g. 127.0.0.1
  --udp_port UDP_PORT   GRC UDP Port, e.g. 16982
  --telemetry_url TELEMETRY_URL
                        SatNOGS DB API Telementry endpoint, e.g. https://db-dev.satnogs.org/api/telemetry/
```

## Testing (without a flowgraph)

You can use the following command to periodically send a static packet via UDP (instead of using your own satellite, station & flowgraph):
```
watch ./qubik_send_frame.py
```

Please note that `qubik_listener.py` will forward frames by default (this can not be disabled!)! Configure both norad ids to the same dummy value, e.g. `99000`, to not litter the Qubik-1 & -2 in db-dev with testframes.

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
